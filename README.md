# aligncenter

## Use align center from html

<div align="center">
  
![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/James_Webb_Space_Telescope.jpg/290px-James_Webb_Space_Telescope.jpg)
</div>

<div align="center">
  
![](fig/JWST_decal.svg)
</div>



## not centered


![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/James_Webb_Space_Telescope.jpg/290px-James_Webb_Space_Telescope.jpg)

![](fig/JWST_decal.svg)
